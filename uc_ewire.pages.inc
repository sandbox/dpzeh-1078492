<?php
// $Id$
/**
 * @file
 * These are the callback pages for the uc_ewire modules.
 * They are used for validating the data Ewire sends back.
 */

function uc_ewire_complete($cart_id = 0) {
    $order_id = arg(3);
    
    $order = uc_order_load($order_id);
    if ($order == FALSE || !empty($_GET['errorMessage'])) {
        switch ($_GET['errorMessage']) {
            case "user canceled the payment":
                drupal_goto('/cart/checkout/review');
                exit;
            /* add some more error handling here */
            default;
                break;
        };
        print t('An error has occurred during payment. Please contact us to ensure your order has submitted.');
        if (!empty($_GET['errorMessage'])) {
            watchdog('EWire', 'Error message for order !order_id: !error_message', array('!order_id' => check_plain($order_id), '!error_message' => check_plain($_GET['errorMessage'])), WATCHDOG_ERROR);
        }
        exit();
    }
    
    // Validate EWire return MD5
    if (md5(variable_get('uc_ewire_encryption_key', '') . $_GET['customerOrderId'] . $_GET['ewireTicket']) != $_GET['validateMD']) {
        print t('An error has occurred during payment. Please contact us to ensure your order has submitted.');
        watchdog('EWire', 'Wrong MD5 check for order !order_id: !error_message', array('!order_id' => check_plain($order_id), '!error_message' => check_plain($_GET['errorMessage'])), WATCHDOG_ERROR);
        exit();
    }
    
    // Log the IPN
    if ($_GET['ewireTicket'] && empty($_GET['errorMessage'])) {
        db_query("INSERT INTO {uc_payment_ewire_ipn} (order_id, ewire_ticket, ewire_action) VALUES (%d, '%s', '%s')",
        $order->order_id, check_plain($_GET['ewireTicket']), 'authorized');
    }
    
    drupal_set_message(t('Your order will be processed as soon as your payment clears at EWire.'));
    uc_order_comment_save($order->order_id, 0, t('Payment is pending approval at EWire.', array(), 'admin'));
    
    uc_cart_empty($cart_id);
    uc_order_save($order);
    
    $url = 'cart/ewire/finalize/'. $order->order_id;
    
    // Javascript redirect on the finalization page.
    $output = '<script type="text/javascript">window.location = "'. url($url, array('absolute' => TRUE)) .'";</script>';

    // Text link for users without Javascript enabled.
    $output .= l(t('Click to complete checkout.'), $url, array('absolute' => TRUE));
    
    print $output;
    exit();
}

function uc_ewire_finalize() {
    $order = uc_order_load(arg(3));

    // Add a comment to let sales team know this came in through the site.
    uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');

    $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));

    $page = variable_get('uc_cart_checkout_complete_page', '');

    if (!empty($page)) {
        drupal_goto($page);
    }

    return $output;
}